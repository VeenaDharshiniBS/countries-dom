const countriesContainer = document.querySelector("[data-country-cards-container]");
const countryTemplate = document.querySelector("[data-country-template]");
const modalTemplate = document.querySelector("[data-modal-template]");

const searchInput = document.querySelector("#search");
const clickFilter = document.querySelector('.click-filter');
const region = document.querySelector('.region-list');
const errorDisplay = document.querySelector('.error-display');
const header = document.querySelector('header');
const searchBar = document.querySelector('.search-bar');

let allCountries = [];

window.addEventListener("load",()=>{
  const loader = document.querySelector(".loader");
  loader.classList.add("loader-hidden");

  loader.addEventListener("transitionend",()=>{
    document.querySelector('body').classList.remove("loader");
  })
})

searchInput.addEventListener("input", searchText => {
  const value = searchText.target.value.toLowerCase();
  allCountries.forEach(eachCountry => {
    const isVisible = eachCountry.name.toLowerCase().includes(value);
    eachCountry.element.classList.toggle("hide", !isVisible);
  })
})

window.addEventListener('click', (event)=>{   
  if (clickFilter.contains(event.target)){
    region.style.transform = "scaleY(1)";
  } else{
    region.style.transform = "scaleY(0)";
  }
});

region.addEventListener("click", (filterInput) => {
  allCountries.forEach(eachCountry => {
    const isVisible = eachCountry.region == filterInput.target.value;
    eachCountry.element.classList.toggle("hide", !isVisible);
  })
})

fetch("https://restcountries.com/v3.1/all")
  .then(res => res.json())
  .then(data => {
    allCountries = data.map(eachCountry => {
      let country = countryTemplate.content.cloneNode(true).children[0];

      let countryFlag = country.querySelector("[data-country-flag]");
      let countryDetails = country.querySelector("[data-country-details]");

      let name = document.createElement('div');
      let population = document.createElement('div');
      let region = document.createElement('div');
      let capital = document.createElement('div');
      let flagUrl = document.createElement('img');
      let nativeName = document.createElement('div');
      let subRegion = document.createElement('div');
      let topLevelDomain = document.createElement('div');
      let currencies = document.createElement('div');
      let languages = document.createElement('div');

      name.innerText = eachCountry.name.common;
      population.innerHTML = `<span class="heading">Population: </span>${eachCountry.population}`;
      region.innerHTML = `<span class="heading">Region: </span>${eachCountry.region}`;
      capital.innerHTML = `<span class="heading">Capital: </span>${eachCountry.capital}`;
      flagUrl.src = eachCountry.flags.svg;
      nativeName.innerHTML = `<span class="heading">Native Name: </span>${getNativeName(eachCountry.name.nativeName)}`;
      subRegion.innerHTML = `<span class="heading">Sub Region: </span>${eachCountry.subregion}`;
      topLevelDomain.innerHTML = `<span class="heading">Top Level Domain: </span>${eachCountry.tld}`;
      currencies.innerHTML = `<span class="heading">Currencies: </span>${getCurrencyName(eachCountry)}`;
      languages.innerHTML = `<span class="heading">Languages: </span>${getLanguages(eachCountry)}`;

      countryFlag.innerHTML = `<img src="${eachCountry.flags.svg}">`;
      // countryFlag.append(flagUrl);
      countryDetails.innerHTML = `<div class="country-name">${eachCountry.name.common}</div>
                                  <div><span class="heading">Population: </span>${eachCountry.population}</div>
                                  <div><span class="heading">Region: </span>${eachCountry.region}</div>
                                  <div><span class="heading">Capital: </span>${eachCountry.capital}</div>`;

      let modal = modalTemplate.content.cloneNode(true).children[0];
      let modalFlag = modal.querySelector("[data-modal-flag]");
      let modalHeader = modal.querySelector("[data-modal-header]");
      let modalDetails = modal.querySelector("[data-modal-details]");
      let modalDetails1 = modal.querySelector("[data-modal-details-1]");
      let modalDetails2 = modal.querySelector("[data-modal-details-2]");
      let modalBorderCountries = modal.querySelector("[data-modal-border-countries]");
      let close = modal.querySelector("[data-modal-close]");

      modalDetails1.append(nativeName, population, region, subRegion, capital);
      modalDetails2.append(topLevelDomain, currencies, languages);
      modalBorderCountries.innerHTML = `<span class="heading">Border Countries</span><div class="modal-border-countries">${getBorderCountries(eachCountry)}</div>`;

      modalFlag.append(flagUrl);

      modalHeader.innerText = eachCountry.name.common;
      modalDetails.append(close);
      country.append(modal);
      countriesContainer.append(country);

      countryFlag.addEventListener("click", () => {
        modal.style.display = "block";
      });

      countryDetails.addEventListener("click", () => {
        modal.style.display = "block";
      });

      close.addEventListener("click", () => {
        modal.style.display = "none";
      });

      window.addEventListener('click', (event) => {
        if (event.target == modal) {
          modal.style.display = 'none';
        }
      });

      document.addEventListener('keydown', function(event) {
        const key = event.key;
        if (key === "Escape") {
          modal.style.display = 'none';
        }
    });

      return { name: eachCountry.name.common, region: eachCountry.region, element: country };
    })
  })
  .catch(()=>{
    errorDisplay.style.display = "block";
    header.style.display = "none";
    searchBar.style.display = "none";
  })

function getNativeName(nativeName) {
  if (typeof nativeName != 'object')
    return " ";

  let ans = [];

  function helper(obj) {
    for (let index = 0; index < Object.entries(obj).length; index++) {
      let value = Object.entries(obj)[index][1];
      let key = Object.entries(obj)[index][0];
      if (typeof value == 'object')
        helper(value);
      else {
        if (key == "common")
          ans.push(value);
      }

    }
  }
  helper(nativeName);
  return ans;
}

function getCurrencyName(country) {
  if(typeof country.currencies != 'object')
    return " ";
  return Object.values(country.currencies)[0].name;
}

function getLanguages(country)
{
  if(typeof country.languages != 'object')
    return " ";
  return Object.values(country.languages);
}

function getBorderCountries(country){
  if(country.hasOwnProperty("borders")==false)
    return " ";
  let res = "";
  for(let b of country.borders)
    res += `<div>${b}</div>`;
  return res;
}